package httputils // import "github.com/docker/docker/api/server/httputils"

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/url"
	"sort"
	"sync"
	"time"

	"github.com/docker/docker/api/types/backend"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/pkg/ioutils"
	"github.com/docker/docker/pkg/stdcopy"
)

// ContainerDecoder specifies how to translate an io.Reader into container configuration.
type ContainerDecoder interface {
	DecodeConfig(src io.Reader) (*container.Config, *container.HostConfig, *network.NetworkingConfig, error)
	DecodeHostConfig(src io.Reader) (*container.HostConfig, error)
}

// WriteLogStream writes an encoded byte stream of log messages from the
// messages channel, multiplexing them with a stdcopy.Writer if mux is true.
func WriteLogStream(ctx context.Context, w io.Writer, msgs <-chan *backend.LogMessage, config *container.LogsOptions, mux bool) {
	// Setup write-flusher and streams
	wf := ioutils.NewWriteFlusher(w)
	defer wf.Close()

	wf.Flush()

	outStream := io.Writer(wf)
	errStream := outStream
	sysErrStream := errStream
	if mux {
		sysErrStream = stdcopy.NewStdWriter(outStream, stdcopy.Systemerr)
		errStream = stdcopy.NewStdWriter(outStream, stdcopy.Stderr)
		outStream = stdcopy.NewStdWriter(outStream, stdcopy.Stdout)
	}

	// Channel to handle concurrency
	var wg sync.WaitGroup
	const maxWorkers = 5
	semaphore := make(chan struct{}, maxWorkers) // limit number of concurrent writes

	for msg := range msgs {
		wg.Add(1)
		semaphore <- struct{}{} // Acquire a slot in the semaphore

		go func(msg *backend.LogMessage) {
			defer wg.Done()
			defer func() { <-semaphore }() // Release the slot

			// Handle potential error in message
			if msg.Err != nil {
				fmt.Fprintf(sysErrStream, "Error grabbing logs: %v\n", msg.Err)
				return
			}

			// Prepare log line
			logLine := msg.Line
			if config.Details {
				logLine = append(attrsByteSlice(msg.Attrs), ' ')
				logLine = append(logLine, msg.Line...)
			}
			if config.Timestamps {
				logLine = append([]byte(msg.Timestamp.Format(time.RFC3339Nano)+" "), logLine...)
			}

			// Handle log output based on source
			if msg.Source == "stdout" && config.ShowStdout {
				_, _ = outStream.Write(logLine)
			}
			if msg.Source == "stderr" && config.ShowStderr {
				_, _ = errStream.Write(logLine)
			}

			// Optionally log to JSON for better integration with log aggregators
			if config.ShowJSON {
				logData := map[string]interface{}{
					"timestamp": msg.Timestamp.Format(time.RFC3339Nano),
					"source":    msg.Source,
					"line":      string(logLine),
					"attrs":     attrsToJSON(msg.Attrs),
				}
				jsonLog, _ := json.Marshal(logData)
				_, _ = outStream.Write(jsonLog)
				_, _ = outStream.Write([]byte("\n")) // Newline for separation
			}
		}(msg)
	}

	// Wait for all goroutines to complete
	wg.Wait()
}

// attrsByteSlice sorts log attributes and converts them to a byte slice.
func attrsByteSlice(a []backend.LogAttr) []byte {
	// Sort attributes by key
	sort.Sort(byKey(a))

	var ret []byte
	for i, pair := range a {
		k, v := url.QueryEscape(pair.Key), url.QueryEscape(pair.Value)
		ret = append(ret, []byte(k)...)
		ret = append(ret, '=')
		ret = append(ret, []byte(v)...)
		if i != len(a)-1 {
			ret = append(ret, ',')
		}
	}
	return ret
}

// byKey is used for sorting log attributes by key.
type byKey []backend.LogAttr

func (b byKey) Len() int           { return len(b) }
func (b byKey) Less(i, j int) bool { return b[i].Key < b[j].Key }
func (b byKey) Swap(i, j int)      { b[i], b[j] = b[j], b[i] }

// attrsToJSON converts log attributes to a JSON structure.
func attrsToJSON(attrs []backend.LogAttr) map[string]string {
	result := make(map[string]string)
	for _, attr := range attrs {
		result[attr.Key] = attr.Value
	}
	return result
}