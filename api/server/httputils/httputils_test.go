package httputils // import "github.com/docker/docker/api/server/httputils"

import (
	"net/http"
	"strings"
	"testing"
	"fmt"
)

// matchesContentType checks if the provided content type matches the expected one.
func TestJsonContentType(t *testing.T) {
	// Valid cases
	err := matchesContentType("application/json", "application/json")
	if err != nil {
		t.Errorf("Error: %v", err)
	}

	err = matchesContentType("application/json; charset=utf-8", "application/json")
	if err != nil {
		t.Errorf("Error: %v", err)
	}

	// Invalid content type with additional unexpected format
	expected := "unsupported Content-Type header (dockerapplication/json): must be 'application/json'"
	err = matchesContentType("dockerapplication/json", "application/json")
	if err == nil || err.Error() != expected {
		t.Errorf("Expected error: %s, got: %v", expected, err)
	}

	// Malformed Content-Type header
	expected = "malformed Content-Type header (foo;;;bar): mime: invalid media parameter"
	err = matchesContentType("foo;;;bar", "application/json")
	if err == nil || err.Error() != expected {
		t.Errorf("Expected error: %s, got: %v", expected, err)
	}
}

// TestReadJSON validates different request body conditions with JSON content.
func TestReadJSON(t *testing.T) {
	// Test case 1: Nil body
	t.Run("nil body", func(t *testing.T) {
		req, err := http.NewRequest(http.MethodPost, "https://example.com/some/path", nil)
		if err != nil {
			t.Fatal("Failed to create request:", err)
		}
		foo := struct{}{}
		err = ReadJSON(req, &foo)
		if err != nil {
			t.Errorf("Unexpected error: %v", err)
		}
	})

	// Test case 2: Empty body
	t.Run("empty body", func(t *testing.T) {
		req, err := http.NewRequest(http.MethodPost, "https://example.com/some/path", strings.NewReader(""))
		if err != nil {
			t.Fatal("Failed to create request:", err)
		}
		foo := struct{ SomeField string }{}
		err = ReadJSON(req, &foo)
		if err != nil {
			t.Errorf("Unexpected error: %v", err)
		}
		if foo.SomeField != "" {
			t.Errorf("Expected empty value, got: %s", foo.SomeField)
		}
	})

	// Test case 3: Valid JSON body
	t.Run("with valid request", func(t *testing.T) {
		req, err := http.NewRequest(http.MethodPost, "https://example.com/some/path", strings.NewReader(`{"SomeField":"some value"}`))
		if err != nil {
			t.Fatal("Failed to create request:", err)
		}
		req.Header.Set("Content-Type", "application/json")
		foo := struct{ SomeField string }{}
		err = ReadJSON(req, &foo)
		if err != nil {
			t.Errorf("Unexpected error: %v", err)
		}
		if foo.SomeField != "some value" {
			t.Errorf("Expected 'some value', got: %s", foo.SomeField)
		}
	})

	// Test case 4: JSON body with extra whitespace
	t.Run("with whitespace", func(t *testing.T) {
		req, err := http.NewRequest(http.MethodPost, "https://example.com/some/path", strings.NewReader(`

		{"SomeField":"some value"}

		`))
		if err != nil {
			t.Fatal("Failed to create request:", err)
		}
		req.Header.Set("Content-Type", "application/json")
		foo := struct{ SomeField string }{}
		err = ReadJSON(req, &foo)
		if err != nil {
			t.Errorf("Unexpected error: %v", err)
		}
		if foo.SomeField != "some value" {
			t.Errorf("Expected 'some value', got: %s", foo.SomeField)
		}
	})

	// Test case 5: JSON body with extra content
	t.Run("with extra content", func(t *testing.T) {
		req, err := http.NewRequest(http.MethodPost, "https://example.com/some/path", strings.NewReader(`{"SomeField":"some value"} and more content`))
		if err != nil {
			t.Fatal("Failed to create request:", err)
		}
		req.Header.Set("Content-Type", "application/json")
		foo := struct{ SomeField string }{}
		err = ReadJSON(req, &foo)
		if err == nil {
			t.Fatal("Expected error for extra content, but got none")
		}
		expected := "unexpected content after JSON"
		if err.Error() != expected {
			t.Errorf("Expected error: '%s', got: %s", expected, err.Error())
		}
	})

	// Test case 6: Invalid JSON
	t.Run("invalid JSON", func(t *testing.T) {
		req, err := http.NewRequest(http.MethodPost, "https://example.com/some/path", strings.NewReader(`{invalid json`))
		if err != nil {
			t.Fatal("Failed to create request:", err)
		}
		req.Header.Set("Content-Type", "application/json")
		foo := struct{ SomeField string }{}
		err = ReadJSON(req, &foo)
		if err == nil {
			t.Fatal("Expected error for invalid JSON, but got none")
		}
		expected := "invalid JSON: invalid character 'i' looking for beginning of object key string"
		if err.Error() != expected {
			t.Errorf("Expected error: '%s', got: %s", expected, err.Error())
		}
	})
}