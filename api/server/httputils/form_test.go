package httputils // import "github.com/docker/docker/api/server/httputils"

import (
	"net/http"
	"net/url"
	"testing"
)

// TestBoolValue tests the BoolValue function, transforming form values into booleans.
func TestBoolValue(t *testing.T) {
	// Define test cases with input values and expected outcomes
	cases := map[string]bool{
		"":      false,  // Empty string should return false
		"0":     false,  // "0" should return false
		"no":    false,  // "no" should return false
		"false": false,  // "false" should return false
		"none":  false,  // "none" should return false
		"1":     true,   // "1" should return true
		"yes":   true,   // "yes" should return true
		"true":  true,   // "true" should return true
		"one":   true,   // "one" should return true
		"100":   true,   // "100" should return true
	}

	// Iterate over test cases for comparison
	for c, expected := range cases {
		// Set up form values and request
		v := url.Values{}
		v.Set("test", c)
		r, _ := http.NewRequest(http.MethodPost, "", nil)
		r.Form = v

		// Call the function and compare the result with expected value
		actual := BoolValue(r, "test")
		if actual != expected {
			t.Fatalf("For input '%s', expected: %v, got: %v", c, expected, actual)
		}
	}
}

// TestBoolValueOrDefault tests BoolValueOrDefault with default value handling.
func TestBoolValueOrDefault(t *testing.T) {
	// Test case when the query parameter is missing
	r, _ := http.NewRequest(http.MethodGet, "", nil)
	if !BoolValueOrDefault(r, "queryparam", true) {
		t.Fatal("Expected default value 'true' but got 'false'")
	}

	// Test case when query parameter is empty
	v := url.Values{}
	v.Set("param", "")
	r, _ = http.NewRequest(http.MethodGet, "", nil)
	r.Form = v
	if BoolValueOrDefault(r, "param", true) {
		t.Fatal("Expected default value 'false' but got 'true'")
	}
}

// TestInt64ValueOrZero tests Int64ValueOrZero function which parses form values into int64.
func TestInt64ValueOrZero(t *testing.T) {
	// Define test cases with input values and expected outcomes
	cases := map[string]int64{
		"":     0,       // empty string should return 0
		"asdf": 0,       // invalid input should return 0
		"0":    0,       // "0" should return 0
		"1":    1,       // "1" should return 1
	}

	// Iterate over test cases for comparison
	for input, expected := range cases {
		v := url.Values{}
		v.Set("test", input)
		r, _ := http.NewRequest(http.MethodPost, "", nil)
		r.Form = v

		actual := Int64ValueOrZero(r, "test")
		if actual != expected {
			t.Fatalf("For input '%s', expected: %v, got: %v", input, expected, actual)
		}
	}
}

// TestInt64ValueOrDefault tests Int64ValueOrDefault with default value handling.
func TestInt64ValueOrDefault(t *testing.T) {
	// Define test cases with inputs and expected results
	cases := map[string]int64{
		"":   -1,    // empty string should return default value (-1)
		"-1": -1,    // valid negative number should return -1
		"42": 42,    // valid positive number should return 42
	}

	// Iterate over test cases for comparison
	for input, expected := range cases {
		v := url.Values{}
		v.Set("test", input)
		r, _ := http.NewRequest(http.MethodPost, "", nil)
		r.Form = v

		actual, err := Int64ValueOrDefault(r, "test", -1)
		if actual != expected {
			t.Fatalf("For input '%s', expected: %v, got: %v", input, expected, actual)
		}
		if err != nil {
			t.Fatalf("Expected no error, but got: %s", err)
		}
	}
}

// TestInt64ValueOrDefaultWithError tests Int64ValueOrDefault with invalid input.
func TestInt64ValueOrDefaultWithError(t *testing.T) {
	// Test case for invalid input
	v := url.Values{}
	v.Set("test", "invalid")
	r, _ := http.NewRequest(http.MethodPost, "", nil)
	r.Form = v

	_, err := Int64ValueOrDefault(r, "test", -1)
	if err == nil {
		t.Fatal("Expected an error but got nil.")
	}
}