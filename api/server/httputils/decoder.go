package httputils // import "github.com/docker/docker/api/server/httputils"

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"

	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	"gopkg.in/yaml.v2"
)

// ContainerDecoder specifies how to translate an io.Reader into container configuration.
type ContainerDecoder interface {
	DecodeConfig(src io.Reader) (*container.Config, *container.HostConfig, *network.NetworkingConfig, error)
	DecodeHostConfig(src io.Reader) (*container.HostConfig, error)
}

// containerDecoder implements the ContainerDecoder interface
type containerDecoder struct{}

// NewContainerDecoder creates a new instance of containerDecoder.
func NewContainerDecoder() ContainerDecoder {
	return &containerDecoder{}
}

// DecodeConfig decodes the full container configuration, including host and networking configuration.
func (cd *containerDecoder) DecodeConfig(src io.Reader) (*container.Config, *container.HostConfig, *network.NetworkingConfig, error) {
	// Decode JSON first
	var configData map[string]interface{}
	decoder := json.NewDecoder(src)
	if err := decoder.Decode(&configData); err != nil {
		// If JSON decoding fails, try YAML
		log.Println("Failed to decode as JSON, attempting YAML:", err)
		src.Seek(0, io.SeekStart) // Reset reader position
		if err := yaml.NewDecoder(src).Decode(&configData); err != nil {
			return nil, nil, nil, fmt.Errorf("failed to decode container configuration from reader: %w", err)
		}
	}

	// Validate essential fields
	if err := validateConfig(configData); err != nil {
		return nil, nil, nil, fmt.Errorf("validation failed: %w", err)
	}

	// Extract and build the container configuration
	containerConfig := &container.Config{
		Image: configData["image"].(string),
		// Add more fields as needed
	}

	hostConfig := &container.HostConfig{
		Memory: int64(configData["memory"].(float64)), // Example for memory
		// Add more fields as needed
	}

	networkingConfig := &network.NetworkingConfig{
		EndpointSpecs: nil, // Customize as necessary
	}

	return containerConfig, hostConfig, networkingConfig, nil
}

// DecodeHostConfig decodes the host configuration from the reader.
func (cd *containerDecoder) DecodeHostConfig(src io.Reader) (*container.HostConfig, error) {
	var hostConfig container.HostConfig
	decoder := json.NewDecoder(src)
	if err := decoder.Decode(&hostConfig); err != nil {
		// If JSON decoding fails, try YAML
		log.Println("Failed to decode as JSON, attempting YAML:", err)
		src.Seek(0, io.SeekStart) // Reset reader position
		if err := yaml.NewDecoder(src).Decode(&hostConfig); err != nil {
			return nil, fmt.Errorf("failed to decode host configuration: %w", err)
		}
	}

	// Validate the host config
	if hostConfig.Memory == 0 {
		return nil, errors.New("host config: memory must be specified")
	}

	return &hostConfig, nil
}

// validateConfig validates the container configuration.
func validateConfig(config map[string]interface{}) error {
	if config["image"] == nil {
		return errors.New("container config: missing required field 'image'")
	}
	// Add more validation rules as necessary
	return nil
}