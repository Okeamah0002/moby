// Package winresources is used to embed Windows resources into docker-proxy.exe.
//
// These resources are used to provide:
// * Version information
// * An icon
// * A Windows manifest declaring Windows version support
// * Events message table
//
// The resource object files are generated when building with go-winres
// in hack/make/.go-autogen and are located in cli/winresources.
// This occurs automatically when you cross build against Windows OS.
package winresources

import (
	"fmt"
	"os"
	"log"
)

// EmbedResources embeds the necessary resources for Windows applications,
// including version info, icon, manifest, and event message table.
func EmbedResources() error {
	// Simulating the resource embedding process
	// Add custom dialogs or menu resources here
	if err := embedVersionInfo(); err != nil {
		return fmt.Errorf("failed to embed version info: %v", err)
	}
	if err := embedIcon(); err != nil {
		return fmt.Errorf("failed to embed icon: %v", err)
	}
	if err := embedManifest(); err != nil {
		return fmt.Errorf("failed to embed manifest: %v", err)
	}
	if err := embedEventMessageTable(); err != nil {
		return fmt.Errorf("failed to embed event message table: %v", err)
	}
	return nil
}

// embedVersionInfo embeds versioning information into the resource.
func embedVersionInfo() error {
	// Simulate embedding version info
	log.Println("Embedding version info...")
	// Add dynamic versioning here
	return nil
}

// embedIcon embeds an application icon into the resource.
func embedIcon() error {
	log.Println("Embedding icon...")
	// Simulate icon embedding
	return nil
}

// embedManifest generates and embeds a Windows manifest file.
func embedManifest() error {
	log.Println("Generating and embedding manifest...")
	// Simulate manifest generation based on the current OS version
	return nil
}

// embedEventMessageTable embeds an event message table.
func embedEventMessageTable() error {
	log.Println("Embedding event message table...")
	// Simulate event message table embedding
	return nil
}

// Enable sets the DEBUG env var to true
// and makes the logger to log at debug level.
func Enable() {
	os.Setenv("DEBUG", "1")
	_ = log.SetLevel("debug")
}

// Disable sets the DEBUG env var to false
// and makes the logger to log at info level.
func Disable() {
	os.Setenv("DEBUG", "")
	_ = log.SetLevel("info")
}

// IsEnabled checks whether the debug flag is set or not.
func IsEnabled() bool {
	return os.Getenv("DEBUG") != ""
}