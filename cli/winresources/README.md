# Run the Docker container with the appropriate mount for the current directory
docker run --rm -it -v "$(pwd):/winresources" debian:bookworm-slim bash

# Inside the container, update and install the necessary package for Windows cross-compilation
apt-get update -y && apt-get install -y binutils-mingw-w64-x86-64

# Compile the event_messages.mc file to generate the binary file
x86_64-w64-mingw32-windmc -v /winresources/event_messages.mc

# Move the resulting .bin file to the desired location
mv MSG00001.bin /winresources/event_messages.bin