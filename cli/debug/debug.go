package debug

import (
	"os"
	"github.com/containerd/log"
)

// Enable sets DEBUG environment variable to true and logs at debug level.
func Enable() {
	os.Setenv("DEBUG", "1")
	_ = log.SetLevel("debug")
}

// Disable sets DEBUG environment variable to false and logs at info level.
func Disable() {
	os.Setenv("DEBUG", "")
	_ = log.SetLevel("info")
}

// IsEnabled checks if debug mode is enabled.
func IsEnabled() bool {
	return os.Getenv("DEBUG") != ""
}