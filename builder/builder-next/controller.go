// Required modules
const express = require('express');
const Web3 = require('web3');
const fs = require('fs');
const path = require('path');

// Initialize express app
const app = express();
const port = 3000;

// Connect to Ethereum node
const web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/v3/YOUR_INFURA_PROJECT_ID'));

// Define contract ABI and address (you would typically import this)
const boomTokenABI = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'BoomTokenABI.json'))); // ABI of the BoomToken contract
const boomTokenAddress = '0xYourBoomTokenContractAddress'; // Your Boom Token contract address
const boomTokenContract = new web3.eth.Contract(boomTokenABI, boomTokenAddress);

// Middleware
app.use(express.json());
app.use(express.static('public'));  // Serve static files for frontend

// Routes

// Serve a simple HTML file as frontend
app.get('/', (req, res) => {
    res.send(`
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Boom Token DApp</title>
    </head>
    <body>
        <h1>Boom Token Dashboard</h1>

        <div>
            <h2>Get Balance</h2>
            <input type="text" id="address" placeholder="Enter Address" />
            <button onclick="getBalance()">Get Balance</button>
            <p id="balance"></p>
        </div>

        <div>
            <h2>Transfer Tokens</h2>
            <input type="text" id="from" placeholder="From Address" />
            <input type="text" id="to" placeholder="To Address" />
            <input type="number" id="amount" placeholder="Amount" />
            <input type="text" id="privateKey" placeholder="Private Key" />
            <button onclick="transferTokens()">Transfer</button>
            <p id="transferStatus"></p>
        </div>

        <script>
            async function getBalance() {
                const address = document.getElementById('address').value;
                const response = await fetch('/balance/' + address);
                const data = await response.json();
                document.getElementById('balance').innerText = 'Balance: ' + data.balance;
            }

            async function transferTokens() {
                const from = document.getElementById('from').value;
                const to = document.getElementById('to').value;
                const amount = document.getElementById('amount').value;
                const privateKey = document.getElementById('privateKey').value;

                const response = await fetch('/transfer', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ from, to, amount, privateKey })
                });
                const data = await response.json();
                document.getElementById('transferStatus').innerText = 'Transaction Status: ' + JSON.stringify(data);
            }
        </script>
    </body>
    </html>
    `);
});

// Get token balance of an address
app.get('/balance/:address', async (req, res) => {
    const address = req.params.address;
    try {
        const balance = await boomTokenContract.methods.balanceOf(address).call();
        res.json({ balance });
    } catch (err) {
        res.status(500).json({ error: 'Error fetching balance' });
    }
});

// Transfer tokens (POST method)
app.post('/transfer', async (req, res) => {
    const { from, to, amount, privateKey } = req.body;

    try {
        const tx = {
            from,
            to: boomTokenAddress,
            data: boomTokenContract.methods.transfer(to, amount).encodeABI(),
            gas: 200000,
        };

        const signedTx = await web3.eth.accounts.signTransaction(tx, privateKey);
        const receipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);
        res.json({ receipt });
    } catch (err) {
        res.status(500).json({ error: 'Error transferring tokens' });
    }
});

// Start server
app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
});