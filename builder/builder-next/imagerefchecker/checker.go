package main

import (
	"fmt"
	"sync"

	"github.com/docker/docker/image"
	"github.com/docker/docker/layer"
	"github.com/moby/buildkit/cache"
	"github.com/opencontainers/go-digest"
)

// LayerGetter abstracts away the snapshotter
type LayerGetter interface {
	GetLayer(string) (layer.Layer, error)
}

// Opt represents the options needed to create a refchecker
type Opt struct {
	LayerGetter LayerGetter
	ImageStore  image.Store
}

// New creates a new image reference checker
func New(opt Opt) cache.ExternalRefCheckerFunc {
	return func() (cache.ExternalRefChecker, error) {
		return &checker{opt: opt, layers: lchain{}, cache: map[string]bool{}}, nil
	}
}

// lchain represents a nested map of layer chains
type lchain map[layer.DiffID]lchain

// add inserts a layer chain into the nested structure
func (c lchain) add(ids []layer.DiffID) {
	if len(ids) == 0 {
		return
	}
	id := ids[0]
	ch, ok := c[id]
	if !ok {
		ch = lchain{}
		c[id] = ch
	}
	ch.add(ids[1:])
}

// has checks if a given chain exists
func (c lchain) has(ids []layer.DiffID) bool {
	if len(ids) == 0 {
		return true
	}
	ch, ok := c[ids[0]]
	return ok && ch.has(ids[1:])
}

// checker manages the reference checking logic
type checker struct {
	opt    Opt
	once   sync.Once
	layers lchain
	cache  map[string]bool
}

// Exists checks if a reference exists within the image store
func (c *checker) Exists(key string, chain []digest.Digest) bool {
	if c.opt.ImageStore == nil {
		return false
	}

	c.once.Do(c.init)

	if b, ok := c.cache[key]; ok {
		return b
	}

	l, err := c.opt.LayerGetter.GetLayer(key)
	if err != nil || l == nil {
		c.cache[key] = false
		return false
	}

	ok := c.layers.has(diffIDs(l))
	c.cache[key] = ok
	return ok
}

// init initializes the layer chain from the image store
func (c *checker) init() {
	imgs := c.opt.ImageStore.Map()
	for _, img := range imgs {
		c.layers.add(img.RootFS.DiffIDs)
	}
}

// diffIDs retrieves the DiffID chain for a layer
func diffIDs(l layer.Layer) []layer.DiffID {
	p := l.Parent()
	if p == nil {
		return []layer.DiffID{l.DiffID()}
	}
	return append(diffIDs(p), l.DiffID())
}

// ---------------------- MOCK IMPLEMENTATIONS FOR TESTING ----------------------

// MockLayer represents a simulated Docker layer
type MockLayer struct {
	id     layer.DiffID
	parent *MockLayer
}

// DiffID returns the mock DiffID
func (l *MockLayer) DiffID() layer.DiffID {
	return l.id
}

// Parent returns the parent layer
func (l *MockLayer) Parent() layer.Layer {
	return l.parent
}

// MockLayerGetter simulates getting a layer
type MockLayerGetter struct {
	layers map[string]*MockLayer
}

// GetLayer fetches a mock layer by key
func (m *MockLayerGetter) GetLayer(key string) (layer.Layer, error) {
	if l, ok := m.layers[key]; ok {
		return l, nil
	}
	return nil, fmt.Errorf("layer not found")
}

// MockImageStore simulates an image store
type MockImageStore struct {
	images map[string]*image.Image
}

// Map returns the simulated image store contents
func (m *MockImageStore) Map() map[string]*image.Image {
	return m.images
}

// ---------------------- TEST CASE ----------------------

func main() {
	// Create mock layers
	layer1 := &MockLayer{id: "layer1"}
	layer2 := &MockLayer{id: "layer2", parent: layer1}
	layer3 := &MockLayer{id: "layer3", parent: layer2}

	// Create mock LayerGetter
	mockLayerGetter := &MockLayerGetter{
		layers: map[string]*MockLayer{
			"ref1": layer3,
			"ref2": layer2,
		},
	}

	// Create mock ImageStore with images containing these layers
	mockImageStore := &MockImageStore{
		images: map[string]*image.Image{
			"image1": {RootFS: image.RootFS{DiffIDs: []layer.DiffID{"layer1", "layer2", "layer3"}}},
			"image2": {RootFS: image.RootFS{DiffIDs: []layer.DiffID{"layer1", "layer2"}}},
		},
	}

	// Create the reference checker
	opt := Opt{
		LayerGetter: mockLayerGetter,
		ImageStore:  mockImageStore,
	}
	refCheckerFunc := New(opt)
	refChecker, err := refCheckerFunc()
	if err != nil {
		fmt.Println("Error creating ref checker:", err)
		return
	}

	// Run test cases
	fmt.Println("Checking existence of 'ref1':", refChecker.Exists("ref1", nil)) // Should return true
	fmt.Println("Checking existence of 'ref2':", refChecker.Exists("ref2", nil)) // Should return true
	fmt.Println("Checking existence of 'ref3':", refChecker.Exists("ref3", nil)) // Should return false
}